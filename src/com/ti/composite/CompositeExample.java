package com.ti.composite;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomasz on 11/24/2015.
 */
public class CompositeExample {

    private static abstract class DocumentElement {
        private Font font;
        private DocumentContainer parent;


        public Font getFont() {
            return font;
        }

        public void setFont(Font font) {
            this.font = font;
        }


        public DocumentContainer getParent() {
            return parent;
        }


        public void setParent(DocumentContainer parent) {
            this.setParent(parent);
        }
    }


    private static class DocChar extends DocumentElement {
        private Font font;
        private DocumentContainer parent;
        private char ch;

        public DocChar(char ch) {
            this.ch = ch;
        }

    }

    private static class DocumentContainer extends DocumentElement {


        private List<DocumentElement> children = new ArrayList<>();



        public DocumentElement getChild(int index) {
            return children.get(index);
        }

        public void addChild(DocumentElement documentElement) {
            children.add(documentElement);
        }

        public void removeChild(DocumentElement documentElement) {
            children.remove(documentElement);
        }
    }

    private static class Document extends DocumentContainer {
        //private List<Page> pages = new ArrayList<>();


        public Page getPage(int index) {
            return (Page) getChild(index);
        }

        public void addPage(Page page) {
            addChild(page);
        }

        public void removePage(Page page) {
            removeChild(page);
        }
    }

    private static class Page extends DocumentContainer {
        public Paragraph getParagraph(int index) {
            return (Paragraph) getChild(index);
        }

        public void addParagraph(Paragraph paragraph) {
            addChild(paragraph);
        }

        public void removeParagraph(Paragraph paragraph) {
            removeChild(paragraph);
        }
        //  private List<Paragraph> paragraphs = new ArrayList<>();
    }

    private static class Paragraph extends DocumentContainer {
        // private List<LineOfText> lines = new ArrayList<>();
        public LineOfText getLineOfText(int index) {
            return (LineOfText) getChild(index);
        }

        public void addLineOfText(LineOfText lineOfText) {
            addChild(lineOfText);
        }

        public void removeLineOfText(LineOfText lineOfText) {
            removeChild(lineOfText);
        }
    }

    private static class LineOfText extends DocumentContainer {
        public DocChar getDocChar(int index) {
            return (DocChar) getChild(index);
        }

        public void addDocChar(DocChar docChar) {
            addChild(docChar);
        }

        public void removeDocChar(DocChar docChar) {
            removeChild(docChar);
        }
    }


    public static void main(String[] args) {
        // create simple document with Ala text
        Document document = new Document();
        Page page = new Page();
        Paragraph paragraph = new Paragraph();
        LineOfText lineOfText = new LineOfText();
        lineOfText.addDocChar(new DocChar('A'));
        lineOfText.addDocChar(new DocChar('l'));
        lineOfText.addDocChar(new DocChar('a'));
        lineOfText.addDocChar(new DocChar(' '));
        lineOfText.addDocChar(new DocChar('m'));
        lineOfText.addDocChar(new DocChar('a'));
        paragraph.addLineOfText(lineOfText);
        page.addParagraph(paragraph);
        document.addPage(page);

    }
}
