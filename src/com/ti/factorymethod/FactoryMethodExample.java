package com.ti.factorymethod;

/**
 * Created by tomasz on 11/23/2015.
 */
public class FactoryMethodExample {

    private DocumentFactory documentFactory;

    public FactoryMethodExample(DocumentFactory documentFactory) {
        this.documentFactory = documentFactory;
    }

    private  final Document newDocument(String type) {
       return  documentFactory.createDocument(type);
    }

    private interface Document {
        String getTite();
        void openDocument();
    }

    private static class MyDocument implements Document {
        @Override
        public String getTite() {
            return "My Document";
        }

        @Override
        public void openDocument() {

        }
    }

    private interface DocumentFactory {
        Document createDocument(String type);
    }

    private static class DocumentFactoryImpl implements  DocumentFactory {

        @Override
        public Document createDocument(String type) {
            return new MyDocument();
        }
    }



    public static void main(String[] args) {
        FactoryMethodExample factoryMethodExample = new  FactoryMethodExample(new DocumentFactoryImpl());
        Document document = factoryMethodExample.newDocument("Bla");
        System.out.println(document);
    }
}
