package com.ti.adapter;

/**
 * Created by tomasz on 11/24/2015.
 */
public class AdapterExample {
    private static class PayPal
    {
        public void sendPayment(float amount) {
            System.out.println("Sending " + amount +" $ through PayPal");
        }
    }
    private interface PaymentAdapter
    {
        void pay(float amount);
    }


    private static class PayPalAdapter implements  PaymentAdapter
    {
        private PayPal payPal;

        public PayPalAdapter(PayPal payPal) {
            this.payPal = payPal;
        }


        public void pay(float amount) {
            payPal.sendPayment(amount);
        }
    }


    public static void main(String[] args) {
//        PayPal payPal = new PayPal();
//        payPal.sendPayment(10.0f);
        PaymentAdapter paypal = new PayPalAdapter(new PayPal());
        paypal.pay(10.0f);

    }
    //interface Payment
}
