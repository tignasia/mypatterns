package com.ti.chainofresponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by tomasz on 11/27/2015.
 */
public class ChainOfResponsibilityExample {
    private static  abstract class PurchasePower {
        protected static final double BASE = 1000;
        protected PurchasePower successor;

        public void setSuccessor(PurchasePower successor) {
            this.successor = successor;
        }

        abstract public void processRequest(int amount);
    }


    private static class ManagerPPower extends PurchasePower {
        private final double ALLOWABLE = 10 * BASE;

        public void processRequest(int amount) {
            if (amount < ALLOWABLE) {
                System.out.println("Manager will approve $" + amount);
            } else if (successor != null) {
                successor.processRequest(amount);
            }
        }
    }

    private static class DirectorPPower extends PurchasePower {
        private final double ALLOWABLE = 20 * BASE;

        public void processRequest(int amount) {
            if (amount < ALLOWABLE) {
                System.out.println("Director will approve $" + amount);
            } else if (successor != null) {
                successor.processRequest(amount);
            }
        }
    }

    private static class VicePresidentPPower extends PurchasePower {
        private final double ALLOWABLE = 40 * BASE;

        public void processRequest(int amount) {
            if (amount < ALLOWABLE) {
                System.out.println("Vice President will approve $" + amount);
            } else if (successor != null) {
                successor.processRequest(amount);
            }
        }
    }

    private static class PresidentPPower extends PurchasePower {
        private final double ALLOWABLE = 60 * BASE;

        public void processRequest(int amount) {
            if (amount < ALLOWABLE) {
                System.out.println("President will approve $" + amount);
            } else {
                System.out.println( "Your request for $" + amount + " needs a board meeting!");
            }
        }
    }

    public static void main(String[] args) {
        ManagerPPower manager = new ManagerPPower();
        DirectorPPower director = new DirectorPPower();
        VicePresidentPPower vp = new VicePresidentPPower();
        PresidentPPower president = new PresidentPPower();
        manager.setSuccessor(director);
        director.setSuccessor(vp);
        vp.setSuccessor(president);

        // Press Ctrl+C to end.
        try {
            while (true) {
                System.out.println("Enter the amount to check who should approve your expenditure.");
                System.out.print(">");
                int d = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
                manager.processRequest(d);
            }
        } catch(Exception e) {
            System.exit(1);
        }
    }


}
