package com.ti.prototype;

/**
 * Created by tomasz on 11/24/2015.
 */
public class PrototypeExample {

    private static abstract class Prototype implements Cloneable {
       public abstract Prototype makeClone();
    }
    private static class ConcretePrototype1 extends Prototype {
        @Override
        public Prototype makeClone () {
            try {
                return (Prototype) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static class ConcretePrototype2 extends Prototype {
        @Override
        public Prototype makeClone () {
            try {
                return (Prototype) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    private static Prototype getNewPrototype()
    {
        // it can be an object returned by some factory ...
        return new ConcretePrototype2();
    }

    public static void main(String[] args) {
       Prototype prototype =   getNewPrototype();
        System.out.println(prototype);
       Prototype clonedPrototype =  prototype.makeClone();
        System.out.println(clonedPrototype);

    }
}
