package com.ti.strategy;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by tomasz on 11/23/2015.
 */
public class StrategyExample2 {

    private static class CalendarDisplay {
        private Holiday holiday;

        public void displayHolidays() {
            holiday.getHolidays(2015).stream().forEach(System.out::println);
        }

        public void setHoliday(Holiday holiday) {
            this.holiday = holiday;
        }
    }
    private interface Holiday {
        List<Date> getHolidays(int year);
    }

    private static class  PolishHolidays implements Holiday {
        @Override
        public List<Date> getHolidays(int year) {
            return Arrays.asList(new Date("11/1/2015"),new Date("11/11/2015"),new Date("12/25/2015"),new Date("12/26/2015"));

        }

    }


    private static class USHolidays implements Holiday {

        @Override
        public List<Date> getHolidays(int year) {
            return Arrays.asList(new Date("11/11/2015"),new Date("11/26/2015"),new Date("12/24/2015"),new Date("12/25/2015"),new Date("12/26/2015"),new Date("12/31/2015"));

        }

    }


    public static void main(String[] args) {
        CalendarDisplay calendarDisplay = new CalendarDisplay();
        System.out.println("POLISH HOLIDAYS");
        calendarDisplay.setHoliday(new PolishHolidays());
        calendarDisplay.displayHolidays();
        System.out.println("US HOLIDAYS");
        calendarDisplay.setHoliday(new USHolidays());
        calendarDisplay.displayHolidays();


    }
}
