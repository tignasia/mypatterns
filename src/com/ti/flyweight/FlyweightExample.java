package com.ti.flyweight;

import java.awt.*;
import java.util.*;

/**
 * Created by tomasz on 11/24/2015.
 */
public class FlyweightExample {

    private static abstract class DocumentElement {

    }

    private static class DocCharFactory {
        Map<Character,DocChar> characters = new LinkedHashMap<>();
        //Set<Character> docChars = new LinkedHashSet<>();

        public DocChar getDocChar(char ch) {
            Character cH = Character.valueOf(ch);
            if(!characters.containsKey(cH)) {
                characters.put(cH,new DocChar(ch));
            }
            return characters.get(cH);
        }
    }
    private static class DocChar extends DocumentElement {
        private char ch;

        public DocChar(char ch) {
            this.ch = ch;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof DocChar) {
                return ch == ((DocChar)obj).ch;
            }
            return false;
        }

        public static int hashCode(char value) {
            return (int)value;
        }

    }

    private static class DocumentContainer extends DocumentElement {

        private java.util.List<DocumentElement> children = new ArrayList<>();
        private Font font;
        private DocumentContainer parent;


        public Font getFont() {
            return font;
        }

        public void setFont(Font font) {
            this.font = font;
        }


        public DocumentContainer getParent() {
            return parent;
        }


        public void setParent(DocumentContainer parent) {
            this.setParent(parent);
        }


        public DocumentElement getChild(int index) {
            return children.get(index);
        }

        public void addChild(DocumentElement documentElement) {
            children.add(documentElement);
        }

        public void removeChild(DocumentElement documentElement) {
            children.remove(documentElement);
        }

        @Override
        public String toString() {
            return parent + " -> " + children;
        }
    }

    private static class Document extends DocumentContainer {
        //private List<Page> pages = new ArrayList<>();


        public Page getPage(int index) {
            return (Page) getChild(index);
        }

        public void addPage(Page page) {
            addChild(page);
        }

        public void removePage(Page page) {
            removeChild(page);
        }
    }

    private static class Page extends DocumentContainer {
        public Paragraph getParagraph(int index) {
            return (Paragraph) getChild(index);
        }

        public void addParagraph(Paragraph paragraph) {
            addChild(paragraph);
        }

        public void removeParagraph(Paragraph paragraph) {
            removeChild(paragraph);
        }
        //  private List<Paragraph> paragraphs = new ArrayList<>();
    }

    private static class Paragraph extends DocumentContainer {
        // private List<LineOfText> lines = new ArrayList<>();
        public LineOfText getLineOfText(int index) {
            return (LineOfText) getChild(index);
        }

        public void addLineOfText(LineOfText lineOfText) {
            addChild(lineOfText);
        }

        public void removeLineOfText(LineOfText lineOfText) {
            removeChild(lineOfText);
        }
    }

    private static class LineOfText extends DocumentContainer {
        public DocChar getDocChar(int index) {
            return (DocChar) getChild(index);
        }

        public void addDocChar(DocChar docChar) {
            addChild(docChar);
        }

        public void removeDocChar(DocChar docChar) {
            removeChild(docChar);
        }

//        @Override
//        public String toString() {
//            return g;
//        }
    }


    public static void main(String[] args) {
        // create simple document with Ala text
        Document document = new Document();
        Page page = new Page();
        Paragraph paragraph = new Paragraph();
        LineOfText lineOfText = new LineOfText();
        DocCharFactory docCharFactory = new  DocCharFactory();
        lineOfText.addDocChar(docCharFactory.getDocChar('A'));
        lineOfText.addDocChar(docCharFactory.getDocChar('l'));
        lineOfText.addDocChar(docCharFactory.getDocChar('a'));
        lineOfText.addDocChar(docCharFactory.getDocChar(' '));
        lineOfText.addDocChar(docCharFactory.getDocChar('m'));
        lineOfText.addDocChar(docCharFactory.getDocChar('a'));
        lineOfText.addDocChar(docCharFactory.getDocChar(' '));
        System.out.println(lineOfText);
      //  null -> [com.ti.flyweight.FlyweightExample$DocChar@74a14482, com.ti.flyweight.FlyweightExample$DocChar@1540e19d, com.ti.flyweight.FlyweightExample$DocChar@677327b6, com.ti.flyweight.FlyweightExample$DocChar@14ae5a5, com.ti.flyweight.FlyweightExample$DocChar@7f31245a, com.ti.flyweight.FlyweightExample$DocChar@677327b6, com.ti.flyweight.FlyweightExample$DocChar@14ae5a5]


//        lineOfText.addDocChar(new DocChar('A'));
//        lineOfText.addDocChar(new DocChar('l'));
//        lineOfText.addDocChar(new DocChar('a'));
//        lineOfText.addDocChar(new DocChar(' '));
//        lineOfText.addDocChar(new DocChar('m'));
//        lineOfText.addDocChar(new DocChar('a'));
        paragraph.addLineOfText(lineOfText);
        page.addParagraph(paragraph);
        document.addPage(page);

    }
}
