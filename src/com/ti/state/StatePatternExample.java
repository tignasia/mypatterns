package com.ti.state;

import java.util.ConcurrentModificationException;

/**
 * Created by tomasz on 2015-11-13.
 */
public  class StatePatternExample {

    private interface State {
        void pressPlay(MP3PlayerContext context);
    }

    //Context
    private static class MP3PlayerContext {
        private State state;

        private MP3PlayerContext(State state) {
            this.state= state;
        }
        public void play() {
            state.pressPlay(this);
        }
        public void setState(State state) {
            this.state = state;
        }
        public State getState() {
            return state;
        }
    }


    private static class StandbyState implements State {
        public void pressPlay(MP3PlayerContext context) {
            context.setState(new PlayingState());
        }
    }
    private static class PlayingState implements State {
        public void pressPlay(MP3PlayerContext context) {
            context.setState(new StandbyState());
        }
    }

    public static void main(String[] args) {
     //  String.format();
        MP3PlayerContext context = new MP3PlayerContext(new StandbyState());
        context.play();


        


     //   context.setState();

     //  MP3PlayerContext context = new MP3PlayerContext(new PlayingState());


    }






}
