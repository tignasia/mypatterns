package com.ti.state;

/**
 * Created by tomasz on 11/23/2015.
 */
public class StatePatternExample2 {

    private static class DoorContext implements  DoorState

    {
        DoorState currentState;

//        public DoorContext(DoorState currentState) {
//            this.currentState = currentState;
//            currentState.setContext(this);
//        }

        private DoorContext() {
            currentState = new OpenedDoor(this);
        //    currentState.setContext(this);
        }


        @Override
        public void open() {
            currentState.open();
        }

        @Override
        public String getStateName() {
            return currentState.getStateName();
        }

//        @Override
//        public void setContext(DoorContext doorContext) {
//
//        }

        @Override
        public void close() {
            currentState.close();
        }


        public String getCurrentStateName() {
            return currentState.getStateName();
        }

        public void setCurrentState(DoorState currentState) {
            this.currentState = currentState;
        }



    }
    private interface DoorState {
        void open();
        void close();
        String getStateName();

      //  void setContext(DoorContext doorContext);
    }

    private static class OpenedDoor implements DoorState {

        private DoorContext doorContext;

        public OpenedDoor(DoorContext doorContext) {
            this.doorContext = doorContext;
        }
        @Override
        public String getStateName() {
            return "Opened";
        }

        @Override
        public void open() {
            // doeas nothing
        }

        @Override
        public void close() {
           doorContext.setCurrentState(new ClosedDoor(doorContext));
        }

//        @Override
//        public void setContext(DoorContext doorContext) {
//            this.doorContext = doorContext;
//        }
    }


    private static class ClosedDoor implements DoorState {

        private DoorContext doorContext;

        public ClosedDoor(DoorContext doorContext) {
            this.doorContext = doorContext;
        }

        @Override
        public String getStateName() {
            return "Closed";
        }

        @Override
        public void open() {
            doorContext.setCurrentState(new OpenedDoor(doorContext));
        }

        @Override
        public void close() {
              doorContext.setCurrentState(new ShutdDoor(doorContext));
        }

//        @Override
//        public void setContext(DoorContext doorContext) {
//            this.doorContext = doorContext;
//        }
    }


    private static class ShutdDoor implements DoorState {

        private DoorContext doorContext;

        public ShutdDoor(DoorContext doorContext) {
            this.doorContext = doorContext;
        }

        @Override
        public String getStateName() {
            return "Shut";
        }

        @Override
        public void open() {
            doorContext.setCurrentState(new ClosedDoor(doorContext));
        }

        @Override
        public void close() {
            // does nohing
        }
//        @Override
//        public void setContext(DoorContext doorContext) {
//            this.doorContext = doorContext;
//        }
    }

    public static void main(String[] args) {
        DoorContext doorContext = new DoorContext();
        doorContext.setCurrentState(new ClosedDoor(doorContext));
        System.out.println(doorContext.getCurrentStateName());
        doorContext.close();
        System.out.println(doorContext.getCurrentStateName());
        doorContext.close();
        System.out.println(doorContext.getCurrentStateName());
        doorContext.open();
        System.out.println(doorContext.getCurrentStateName());
        doorContext.open();
        System.out.println(doorContext.getCurrentStateName());
        doorContext.open();
        System.out.println(doorContext.getCurrentStateName());

    }
}
