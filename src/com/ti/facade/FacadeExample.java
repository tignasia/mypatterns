package com.ti.facade;

/**
 * Created by tomasz on 11/24/2015.
 */
public class FacadeExample {
    /* Complex parts */

    private static class CPU {
        public void freeze() {
            System.out.println("CPU.freeze()");
        }

        public void jump(long position) {
            System.out.println("CPU.jump(" + position + ")");
        }

        public void execute() {
            System.out.println("CPU.execute()");
        }
    }

    private static class Memory {
        public void load(long position, byte[] data) {
            System.out.println("Memory.load(" + position + "," + data.length + ")");
        }
    }

    private static class HardDrive {
        public byte[] read(long lba, int size) {
            System.out.println("HardDrive.read(" + lba + "," + size + ")");
            return new byte[0];
        }
    }

/* Facade */

    private static class ComputerFacade {
        private CPU processor;
        private Memory ram;
        private HardDrive hd;

        public ComputerFacade() {
            this.processor = new CPU();
            this.ram = new Memory();
            this.hd = new HardDrive();
        }

        public void start() {
            processor.freeze();
            ram.load(0, hd.read(1, 128));
            processor.jump(0);
            processor.execute();
        }
    }

/* Client */

    public static void main(String[] args) {
        ComputerFacade computer = new ComputerFacade();
        computer.start();
    }
}
