package com.ti.visitor;

/**
 * Created by tomasz on 11/25/2015.
 */
public class VIsitorExample {

    interface CarElementVisitor {
        void visit(Wheel wheel);
        void visit(Engine engine);
        void visit(Body body);
        void visit(Car car);
    }



    interface CarElement {
        void accept(CarElementVisitor visitor);
    }


    private static  class Wheel implements CarElement {
        private String name;

        public Wheel(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public void accept(CarElementVisitor visitor) {
            visitor.visit(this);
        }
    }

    private static  class Engine implements CarElement {
        public void accept(CarElementVisitor visitor) {
            visitor.visit(this);
        }
    }

    private static  class Body implements CarElement {
        public void accept(CarElementVisitor visitor) {
            visitor.visit(this);
        }
    }

    private static  class Car implements CarElement {
        CarElement[] elements;

        public Car() {
            this.elements = new CarElement[] { new Wheel("front left"),
                    new Wheel("front right"), new Wheel("back left") ,
                    new Wheel("back right"), new Body(), new Engine() };
        }

        public void accept(CarElementVisitor visitor) {
            for(CarElement elem : elements) {
                elem.accept(visitor);
            }
            visitor.visit(this);
        }
    }


    private static  class CarElementPrintVisitor implements CarElementVisitor {
        public void visit(Wheel wheel) {
            System.out.println("Visiting " + wheel.getName() + " wheel");
        }

        public void visit(Engine engine) {
            System.out.println("Visiting engine");
        }

        public void visit(Body body) {
            System.out.println("Visiting body");
        }

        public void visit(Car car) {
            System.out.println("Visiting car");
        }
    }

    private static  class CarElementDoVisitor implements CarElementVisitor {
        public void visit(Wheel wheel) {
            System.out.println("Kicking my " + wheel.getName() + " wheel");
        }

        public void visit(Engine engine) {
            System.out.println("Starting my engine");
        }

        public void visit(Body body) {
            System.out.println("Moving my body");
        }

        public void visit(Car car) {
            System.out.println("Starting my car");
        }
    }




    public static void main(String[] args) {
        CarElement car = new Car();
        car.accept(new CarElementPrintVisitor());
        car.accept(new CarElementDoVisitor());
    }
}
