package com.ti.interpreter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by tomasz on 11/27/2015.
 */
public class InterpreterExample {

    public static class InterpreterContext {

        public String getBinaryFormat(int i){
            return Integer.toBinaryString(i);
        }

        public String getHexadecimalFormat(int i){
            return Integer.toHexString(i);
        }
    }

    public interface Expression {

        String interpret(InterpreterContext ic);
    }

    public static class IntToBinaryExpression implements Expression {

        private int i;

        public IntToBinaryExpression(int c){
            this.i=c;
        }
        @Override
        public String interpret(InterpreterContext ic) {
            return ic.getBinaryFormat(this.i);
        }

    }

    public static class IntToHexExpression implements Expression {

        private int i;

        public IntToHexExpression(int c){
            this.i=c;
        }

        @Override
        public String interpret(InterpreterContext ic) {
            return ic.getHexadecimalFormat(i);
        }

    }


    private static  InterpreterContext ic = new InterpreterContext();


    public static String interpret(String str){
        Expression exp=null;
        //create rules for expressions
        if(str.contains("Hexadecimal")){
            exp=new IntToHexExpression(Integer.parseInt(str.substring(0,str.indexOf(" "))));
        }else if(str.contains("Binary")){
            exp=new IntToBinaryExpression(Integer.parseInt(str.substring(0,str.indexOf(" "))));
        }else return str;

        return exp.interpret(ic);
    }

    public static void main(String args[]){
        String str1 = "56 in Binary";
        String str2 = "56 in Hexadecimal";
        System.out.println(str1+"= "+ interpret(str1));
        System.out.println(str2+"= "+ interpret(str2));

    }
}
