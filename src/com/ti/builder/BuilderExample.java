package com.ti.builder;

/**
 * Created by tomasz on 11/23/2015.
 */
public class BuilderExample {
  // we want to avoid situation with many constructor wirh different parameter sets.
    //EmailMessage emailMessage =  new EmailMessage("gojko@example.com", "me@crisp.se", "hello comrade", "some content");
  // Solution - Builder Pattern

    EmailMessage emailMessage = EmailMessage.builder()
                                            .from("gojko@example.com")
                                            .to("me@crisp.se")
                                            .subject("hello comrade")
                                            .content("Some content")
                                            .build();

}
