package com.ti.builder;

/**
 * Created by tomasz on 11/23/2015.
 */
public class EmailMessage {
    private String from;
    private String to;
    private String subject;
    private String content;
    private String mimeType;  // optional


    // Constructor with default mime type
//    public EmailMessage(String from, String to, String subject, String content) {
//        this(from, to, subject, content, null);
//    }
//
//    public EmailMessage(String from, String to, String subject, String content, String mimeType) {
//        this.from = from;
//        this.to = to;
//        this.subject = subject;
//        this.content = content;
//        this.mimeType = mimeType;
//    }


    public static Builder builder() {
        return new EmailMessage.Builder();
    }

    public static class Builder {
        private EmailMessage instance = new EmailMessage();

        public Builder() {
        }

        public Builder from(String from) {
            instance.from = from;
            return this;
        }

        public Builder to(String to) {
            instance.to = to;
            return this;
        }

        public Builder subject(String subject) {
            instance.subject = subject;
            return this;
        }

        public Builder content(String content) {
            instance.content = content;
            return this;
        }

        public Builder mimeType(String mimeTypeName) {
            instance.mimeType = mimeTypeName;
            return this;
        }

        public EmailMessage build() {
            return instance;
        }
    }
}
