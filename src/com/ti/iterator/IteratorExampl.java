package com.ti.iterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by tomasz on 11/24/2015.
 */
public class IteratorExampl {

    private static List<String> names = Arrays.asList("Tomasz","Joanna","Kajetan","Maja");

    private static class MyIterator implements Iterator<String>  {
        int pos = 0;

        @Override
        public boolean hasNext() {
            return names.size() > pos;
        }

        @Override
        public String next() {
            return names.get(pos++);
        }
    }

    public static void main(String[] args) {
            MyIterator iterator = new MyIterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
    }
}
