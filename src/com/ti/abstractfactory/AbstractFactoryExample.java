package com.ti.abstractfactory;

/**
 * Created by tomasz on 11/23/2015.
 */
public class AbstractFactoryExample {


    private  static DocumentFactory getDocumentFactory(String type){
        switch (type) {
            case  "My" : return new MyDocumentFactory();
            case "Your" : return  new YourDocumentFactory();
        }
        return  null;
    }

    private interface DocumentFactory {
        Document newDocument(String type);
    }

    private static class MyDocumentFactory implements DocumentFactory {
        @Override
        public Document newDocument(String type) {
            return new MyDocument();
        }
    }

    private static class YourDocumentFactory implements DocumentFactory {
        @Override
        public Document newDocument(String type) {
            return new YourDocument();
        }
    }

//    private DocumentFactory documentFactory;

//    public AbstractFactoryExample(DocumentFactory documentFactory) {
//        this.documentFactory = documentFactory;
//    }
//
//    private  final Document newDocument(String type) {
//       return  documentFactory.createDocument(type);
//    }

    private interface Document {
        String getTite();
        void openDocument();
    }

    private static class MyDocument implements Document {
        @Override
        public String getTite() {
            return "My Document";
        }

        @Override
        public void openDocument() {

        }
    }

    private static class YourDocument implements Document {
        @Override
        public String getTite() {
            return "Your Document";
        }

        @Override
        public void openDocument() {

        }
    }


    public static void main(String[] args) {
        DocumentFactory documentFactory = AbstractFactoryExample.getDocumentFactory("My");

        Document document = documentFactory.newDocument("Bla");
        System.out.println(document);

         documentFactory = AbstractFactoryExample.getDocumentFactory("Your");

        document = documentFactory.newDocument("Bla");
        System.out.println(document);
    }
}
