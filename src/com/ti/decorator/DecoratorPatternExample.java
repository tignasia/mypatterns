package com.ti.decorator;

/**
 * Created by tomasz on 2015-11-12.
 */
public class DecoratorPatternExample {

    // The abstract Coffee class defines the functionality of Coffee implemented by decorator
     private static abstract class Coffee {
        public abstract double getCost(); // Returns the cost of the coffee
        public abstract String getIngredients(); // Returns the ingredients of the coffee
    }

    // Extension of a simple coffee without any extra ingredients
    private static class SimpleCoffee extends Coffee {
        public double getCost() {
            return 1;
        }

        public String getIngredients() {
            return "Coffee";
        }
    }


    /// Abstract decorator class - note that it extends Coffee abstract class
    public abstract static class CoffeeDecorator extends Coffee {
        protected final Coffee decoratedCoffee;

        public CoffeeDecorator(Coffee c) {
            this.decoratedCoffee = c;
        }

        public double getCost() { // Implementing methods of the abstract class
            return decoratedCoffee.getCost();
        }

        public String getIngredients() {
            return decoratedCoffee.getIngredients();
        }
    }

    // Decorator WithMilk mixes milk into coffee.
// Note it extends CoffeeDecorator.
    private static class WithMilk extends CoffeeDecorator {
        public WithMilk(Coffee c) {
            super(c);
        }

        public double getCost() { // Overriding methods defined in the abstract superclass
            return super.getCost() + 0.5;
        }

        public String getIngredients() {
            return super.getIngredients() + ", Milk";
        }
    }

    // Decorator WithSprinkles mixes sprinkles onto coffee.
// Note it extends CoffeeDecorator.
    private static  class WithSprinkles extends CoffeeDecorator {
        public WithSprinkles(Coffee c) {
            super(c);
        }

        public double getCost() {
            return super.getCost() + 0.2;
        }

        public String getIngredients() {
            return super.getIngredients() + ", Sprinkles";
        }
    }

    public static void printInfo(Coffee c) {
        System.out.println("Cost: " + c.getCost() + "; Ingredients: " + c.getIngredients());
    }

    public static void main(String[] args) {
        Coffee c = new SimpleCoffee();
        printInfo(c);

        c = new WithMilk(c);
        printInfo(c);

        c = new WithSprinkles(c);
        printInfo(c);
    }

}

