package com.ti.decorator;

/**
 * Created by tomasz on 11/23/2015.
 */
public class DecoratorPatternExample2 {
   private interface Window {
       void draw();
   }

    private static class SimpleWindow implements  Window{

        @Override
        public void draw() {
            System.out.print("Draws standard window");
        }
    }

    private static abstract class WindowDecorator implements Window {
        protected Window windowToBeDecorated;

        public WindowDecorator(Window windowToBeDecorated) {
            this.windowToBeDecorated = windowToBeDecorated;
        }


        @Override
        public void draw() {
            windowToBeDecorated.draw();
        }
    }

    private static class HorizontalScrollbarDecortor extends  WindowDecorator {

        public HorizontalScrollbarDecortor(Window windowToBeDecorated) {
            super(windowToBeDecorated);
        }

        @Override
        public void draw() {
            super.draw();
            drawHorizontalScrollBar();
        }

        private void drawHorizontalScrollBar() {
            System.out.println(" with Horizontal Scrollbar ....");
        }
    }


    private static class VerticalScrollbarDecortor extends  WindowDecorator {

        public VerticalScrollbarDecortor(Window windowToBeDecorated) {
            super(windowToBeDecorated);
        }

        @Override
        public void draw() {
            super.draw();
            drawVerticalScrollBar();
        }

        private void drawVerticalScrollBar() {
            System.out.println(" with Vertical Scrollbar ....");
        }
    }



    public static void main(String[] args) {
        Window hWindow = new HorizontalScrollbarDecortor(new SimpleWindow());
        hWindow.draw();
        Window vWindow = new VerticalScrollbarDecortor(new SimpleWindow());
        vWindow.draw();
        Window hvWindow =  new HorizontalScrollbarDecortor(new VerticalScrollbarDecortor(new SimpleWindow()));
        hvWindow.draw();
    }
}
