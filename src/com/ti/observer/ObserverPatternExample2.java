package com.ti.observer;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import static java.lang.System.out;

/**
 * Created by tomasz on 11/23/2015.
 */
public class ObserverPatternExample2 {
    private static class EventSource extends Observable implements Runnable {

        @Override
        public void run() {
            while (true) {
                Scanner scaner = new Scanner(System.in);
                String token = scaner.next();
                setChanged();
                notifyObservers(token);
            }

        }
    }

    public static void main(String[] args) {
        EventSource eventSource = new EventSource();
        for (int i = 0; i < 5; i++) {
            final int finalI = i;
            eventSource.addObserver((Observable o, Object arg) -> {
                System.out.println("\n Observer " + finalI + " Received notification : " + arg);
            });
//
//            eventSource.addObserver(new Observer() {
//                @Override
//                public void update(Observable o, Object arg) {
//                    System.out.println("\n Observer " + finalI + " Received notification : " + arg);
//                }
//            });
        }

        new Thread(eventSource).start();
    }
}
