package com.ti.observer;

/**
 * Created by tomasz on 2015-11-12.
 */

import java.util.Observable;
import static java.lang.System.out;
import java.util.Observable;
import java.util.Scanner;

public class ObserverPatternExample {


    private static class EventSource extends Observable implements Runnable {
        public void run() {
            while (true) {
                String response = new Scanner(System.in).next();
                setChanged();
                notifyObservers(response);
            }
        }
    }

    public static void main(String[] args) {
        out.println("Enter Text >");
        EventSource eventSource = new EventSource();

        for (int i = 0; i < 5 ; i++) {
            final int finalI = i;
            eventSource.addObserver((Observable obj, Object arg) -> {
                out.println("\n Observer " + finalI + " akReceived realasponse: " + arg);
            });
        }


        new Thread(eventSource).start();
    }
}
