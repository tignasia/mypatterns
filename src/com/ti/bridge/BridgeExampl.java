package com.ti.bridge;

/**
 * Created by tomasz on 11/24/2015.
 */
public class BridgeExampl {
    /**
     * "Implementor"
     */
    interface DrawingAPI {

        public void drawCircle(double x, double y, double radius);

        public void drawRectange(double x, double y, double width, double height);
    }


    private static class DrawingJaxaFxAPI implements DrawingAPI {
        @Override
        public void drawCircle(double x, double y, double radius) {
            System.out.println("JavaFX drawCircle(" + x + "," + y + "," + radius + ")");
        }

        @Override
        public void drawRectange(double x, double y, double width, double height) {
            System.out.println("JavaFX drawRectange(" + x + "," + y + "," + width + "," + height + ")");
        }
    }


    private static class DrawingJaxa2DAPI implements DrawingAPI {
        @Override
        public void drawCircle(double x, double y, double radius) {
            System.out.println("Java2D drawCircle(" + x + "," + y + "," + radius + ")");
        }

        @Override
        public void drawRectange(double x, double y, double width, double height) {
            System.out.println("Java2D drawRectange(" + x + "," + y + "," + width + "," + height + ")");
        }
    }

    /**
     * "Abstraction wich uses Implementor"
     */
    private abstract static class Shape {
        protected DrawingAPI drawingAPI;

        protected Shape(DrawingAPI drawingAPI) {
            this.drawingAPI = drawingAPI;
        }

        public abstract void draw();                             // low-level

        public abstract void resizeByPercentage(double pct);     // high-level
    }

    private static class CircleShape extends Shape {
        private double x, y, radius;

        public CircleShape(double x, double y, double radius, DrawingAPI drawingAPI) {
            super(drawingAPI);
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        // low-level i.e. Implementation specific
        public void draw() {
            drawingAPI.drawCircle(x, y, radius);
        }

        // high-level i.e. Abstraction specific
        public void resizeByPercentage(double pct) {
            radius *= (1.0 + pct / 100.0);
        }
    }

    private static class RectangeShape extends Shape {
        private double x, y, width, height;

        public RectangeShape(double x, double y, double width, double height, DrawingAPI drawingAPI) {
            super(drawingAPI);
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        // low-level i.e. Implementation specific
        public void draw() {
            drawingAPI.drawRectange(x,y,width,height);
        }

        // high-level i.e. Abstraction specific
        public void resizeByPercentage(double pct) {
            width *= (1.0 + pct / 100.0);
            height *= (1.0 + pct / 100.0);
        }
    }

    public static void main(String[] args) {
        Shape[] shapes = new Shape[] {
                new CircleShape(1, 2, 3, new DrawingJaxa2DAPI()),
                new CircleShape(5, 7, 11, new DrawingJaxaFxAPI()),
                new RectangeShape(3, 5, 10, 5, new DrawingJaxa2DAPI()),
                new RectangeShape(3, 5, 10, 5, new DrawingJaxaFxAPI()),
        };

        for (Shape shape : shapes) {
            shape.resizeByPercentage(50);
            shape.draw();
        }
    }


}
