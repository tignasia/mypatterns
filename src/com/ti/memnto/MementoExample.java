package com.ti.memnto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomasz on 11/27/2015.
 */
public class MementoExample {

// Object for wich the state must be restorable ( rolled-back from Memento)
    private static class Originator {
        private String state;
        // The class could also contain additional data that is not part of the
        // state saved in the memento..

        public void set(String state) {
            System.out.println("Originator: Setting state to " + state);
            this.state = state;
        }

        public Memento saveToMemento() {
            System.out.println("Originator: Saving to Memento.");
            return new Memento(this.state);
        }

        public void restoreFromMemento(Memento memento) {
            this.state = memento.getSavedState();
            System.out.println("Originator: State after restoring from Memento: " + state);
        }
    }

    // Memento should be immutable

    private static final class Memento {
        private final String state;

        public Memento(String stateToSave) {
            state = stateToSave;
        }

        public String getSavedState() {
            return state;
        }
    }

/// MementoExample acts as caretaker -    List<Originator.Memento> savedStates = new ArrayList<Originator.Memento>();

    public static void main(String[] args) {
        List<Memento> savedStates = new ArrayList<>(); //History
        Originator originator = new Originator();
        originator.set("State1");
        originator.set("State2");
        savedStates.add(originator.saveToMemento());
        originator.set("State3");
        // We can request multiple mementos, and choose which one to roll back to.
        originator.set("State4");
        savedStates.add(originator.saveToMemento());
        originator.restoreFromMemento(savedStates.get(0));
        originator.restoreFromMemento(savedStates.get(1));
    }


}
